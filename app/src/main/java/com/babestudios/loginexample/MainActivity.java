package com.babestudios.loginexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

	private LoginButton loginButton;
	private CallbackManager callbackManager;
	private PrefUtil prefUtil;
	private ImageView iv;
	private TextView tvName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		callbackManager = CallbackManager.Factory.create();
		setContentView(R.layout.activity_main);
		loginButton = (LoginButton) findViewById(R.id.facebook_login);
		tvName = (TextView) findViewById(R.id.tv_name);
		iv = (ImageView) findViewById(R.id.iv);
		loginButton.setReadPermissions(Arrays.asList(
				"public_profile", "email"));
		prefUtil = new PrefUtil(this);

		loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
					@Override
					public void onSuccess(LoginResult loginResult) {


						String accessToken = loginResult.getAccessToken().getToken();

						// save accessToken to SharedPreference
						prefUtil.saveAccessToken(accessToken);

						GraphRequest request = GraphRequest.newMeRequest(
								loginResult.getAccessToken(),
								new GraphRequest.GraphJSONObjectCallback() {
									@Override
									public void onCompleted(JSONObject jsonObject,
															GraphResponse response) {

										// Getting FB User Data
										Bundle facebookData = getFacebookData(jsonObject);


									}
								});

						Bundle parameters = new Bundle();
						parameters.putString("fields", "id,first_name,last_name,email,gender");
						request.setParameters(parameters);
						request.executeAsync();
					}


					@Override
					public void onCancel () {
						Log.d("test", "Login attempt cancelled.");
					}

					@Override
					public void onError (FacebookException e){
						e.printStackTrace();
						Log.d("test", "Login attempt failed.");
						deleteAccessToken();
					}
				}
		);
	}

	private void deleteAccessToken() {
		AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
			@Override
			protected void onCurrentAccessTokenChanged(
					AccessToken oldAccessToken,
					AccessToken currentAccessToken) {

				if (currentAccessToken == null){
					//User logged out
					prefUtil.clearToken();
					LoginManager.getInstance().logOut();
				}
			}
		};
	}
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}

	private Bundle getFacebookData(JSONObject object) {
		Bundle bundle = new Bundle();

		try {
			String id = object.getString("id");
			URL profile_pic;
			try {
				profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?type=large");
				Picasso.with(this).load(profile_pic.toString()).into(iv);
				Log.i("profile_pic", profile_pic + "");
				bundle.putString("profile_pic", profile_pic.toString());
			} catch (MalformedURLException e) {
				e.printStackTrace();
				return null;
			}

			bundle.putString("idFacebook", id);
			if (object.has("first_name"))
				bundle.putString("first_name", object.getString("first_name"));
			if (object.has("last_name"))
				bundle.putString("last_name", object.getString("last_name"));
			if (object.has("email"))
				bundle.putString("email", object.getString("email"));
			if (object.has("gender"))
				bundle.putString("gender", object.getString("gender"));

			tvName.setText(object.getString("first_name") + " " + object.getString("last_name") + " " + object.getString("email") + " " + object.getString("gender"));

			prefUtil.saveFacebookUserInfo(object.getString("first_name"),
					object.getString("last_name"),object.getString("email"),
					object.getString("gender"), profile_pic.toString());

		} catch (Exception e) {
			Log.d("test", "BUNDLE Exception : "+e.toString());
		}

		return bundle;
	}
}
